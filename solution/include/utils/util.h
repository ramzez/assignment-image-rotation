#include <stdio.h>

enum open_status { OPEN_OK = 0, OPEN_FAILED};

enum open_status open_file(FILE **file, const char *path, const char *modes);

int close_file(FILE *file);
