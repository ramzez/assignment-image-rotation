#include "../../include/image/image.h"

struct image *rotate(struct image const *source){
  struct image *rotated = create(source->height, source->width);
  for(size_t y = 0; y<source->height; y++){
    for(size_t x = 0; x < source->width; x++){
      rotated->data[rotated->width * x + source->height - 1 - y] = source->data[source->width * y + x];
    }
  }
  return rotated;
}
